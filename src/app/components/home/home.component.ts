import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'Welcome to MusicMind';
  desc = 'Here you can find all kinds of music';
  constructor(private spotify: SpotifyService) {
    this.spotify.getUserProfile().subscribe(
      (data: any) => {
        console.log(data);
      }, (errorServicio) => {
        console.log(errorServicio);
      }
    );
   }

  ngOnInit() {
    $('.parallax').parallax();
  }

}
