import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login =  'Login';
  email = 'email';
  password = 'password';
  formdata;
  emailId;

  constructor(private router: Router) { }

  ngOnInit() {
    this.formdata = new FormGroup({
       email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('[^ @]*@[^ @]*')
       ])),
       pass: new FormControl('', this.passwordvalidation)
    });
 }
 passwordvalidation(formcontrol) {
    if (formcontrol.value.length < 5) {
       return {'pass' : true};
    }
 }
 onClickSubmit(data) {
    this.emailId = data.email;
    this.router.navigate(['/home']);
  }

}
