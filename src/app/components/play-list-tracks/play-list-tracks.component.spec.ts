import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayListTracksComponent } from './play-list-tracks.component';

describe('PlayListTracksComponent', () => {
  let component: PlayListTracksComponent;
  let fixture: ComponentFixture<PlayListTracksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayListTracksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayListTracksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
