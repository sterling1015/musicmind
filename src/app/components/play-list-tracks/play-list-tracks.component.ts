import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';
import * as $ from 'jquery';
declare var $: any;

@Component({
  selector: 'app-play-list-tracks',
  templateUrl: './play-list-tracks.component.html',
  styleUrls: ['./play-list-tracks.component.css']
})
export class PlayListTracksComponent implements OnInit {
  tracks: any[] = [];
  config: any;
  follow = 'Followers';
  popularity = 'popularity';
  songTitle;
  albumName;
  artistname;
  playListName;
  playListImg;
  playListOwner;
  playListFollowers;
  playListType;
  //Modal
  modalHeader;
  modalDesc;
  modalId;
  modalImg;
  modalPopularity;
  constructor(private router: ActivatedRoute, private spotify: SpotifyService) {
    this.router.params.subscribe( params => {
     this.getTracksTolist(params['id'] );
     this.getPlayListSelected(params['id'] );
    });
    // Config pagination
    this.config = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: this.tracks.length
    };
   }

  ngOnInit() {
    $('td').children('.materialboxed').materialbox();
    $('.materialboxed').materialbox();
    $('.modal').modal();
  }
  // Get user tracks by playList
  getTracksTolist(id: string) {
    this.spotify.getTracksTolist(id).subscribe((data: any) => {
    this.tracks = data.items;
    console.log(this.tracks);
    });

  }

  // Pagination
  pageChanged(event) {
    this.config.currentPage = event;
  }

  // Show laylist selected
  getPlayListSelected(id: string) {
    this.spotify.getPlayListSelected(id).subscribe((data: any) => {
      this.playListName = data.name;
      this.playListImg = data.images[0].url;
      this.playListOwner = data.owner.display_name;
      this.playListFollowers = data.followers.total;
      this.playListType = data.type;
    });

  }

// Show modal with parameters
  showModal(id: string, name: string, album: string, img: string, popularity: string) {
    console.log(id + ' ' + name + ' ' + album);
    this.modalHeader = name;
    this.modalDesc = album;
    this.modalId = id;
    this.modalImg = img;
    this.modalPopularity = popularity;
  }

}
