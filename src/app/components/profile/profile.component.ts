import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { SpotifyService } from 'src/app/services/spotify.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  title = 'My profile';
  MyInfo = 'My info';
  Followers = 'Followers :';
  MyPlayList = 'Playlist';
  playList: any[] = [];
  userProfile: any[] = [];
  userType;
  userImg;
  userName;
  Userfollowers;
  url;
  constructor(private spotify: SpotifyService, private router: Router) { }

  ngOnInit() {
    $('.tabs').tabs();
    this.getUserPlayList();
    this.getUserProfile();
  }
 // Get user playList by Id
  getUserPlayList() {
    this.spotify.getUserPlayList().subscribe( (data: any) => {
      this.playList = data.items;
    });

  }
  // Get user profile by Id
  getUserProfile() {
    this.spotify.getUserProfile().subscribe( (data: any) => {
      this.userType = data.type;
      this.userImg = data.images[0].url;
      this.userName = data.display_name;
      this.Userfollowers = data.followers.total;
      this.url = data.external_urls.spotify;
      this.userProfile = data;
      console.log(data);
    });

  }
  // Get user tracks by playList
  showPlayList(item: any) {
    this.spotify.getTracksTolist(item).subscribe((data: any) => {
      this.router.navigate(['/playListTracks', item]);
    });
  }

}
