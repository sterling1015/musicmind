import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  tracks: any[] = [];
  constructor(private spotify: SpotifyService) {}

  // Get tracks for ID
  searchTracks(term: string) {
    console.log(term);
    this.spotify.getTracks(term).subscribe( (data: any) => {
      console.log(data);
      this.tracks = data;
    });

  }


  ngOnInit() {
  }


}
