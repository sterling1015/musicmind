import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.css']
})
export class TracksComponent implements OnInit {
  config: any;
  @Input() items: any[] = [];
  isValid = false;
  constructor(private router: Router ) {
    // Config pagination
    this.config = {
      itemsPerPage: 3,
      currentPage: 1,
      totalItems: this.items.length
    };
  }

  // Pagination
  pageChanged(event) {
    this.config.currentPage = event;
  }

  ngOnInit() {
  }

}
