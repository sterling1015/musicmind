import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { }

  getQuery(query: string) {
    const url = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      Authorization:
        'Bearer BQCFui0DYWR-B68lk1UBtyRVSKCKMmSRao4P-OO-QfJM8ckysRUnumJlroTSmoPWNe-EM5yiBCrRyQjlbiE'

    });

    return this.http.get(url, { headers });
  }

  // Get user profile by Id
  getUserProfile() {
    return this.getQuery(`users/22nd2krudunwo44pjn54ucfyy`);
  }
  // Get tracks by Id
  getTracks(termino: string) {
    return this.getQuery(`search?q=${termino}&type=track&limit=15`).pipe(
      map(data => data['tracks'].items)
    );

  }
 // Get user playList by Id
  getUserPlayList() {
    return this.getQuery(`users/22nd2krudunwo44pjn54ucfyy/playlists?limit=10`);
  }
  // Get user tracks by playList
  getTracksTolist(item: string) {
    return this.getQuery(`playlists/${item}/tracks?fields=items(added_by.id%3Btrack(name%2Chref%2Calbum(name%2Chref)))&limit=10&offset=5`);
  }

  // Get playList by ID
  getPlayListSelected(id: string) {
    return this.getQuery(`playlists/${id}?fields=items(added_by.id%3Btrack(name%2Chref%2Calbum(name%2Chref)))`);

  }


}
